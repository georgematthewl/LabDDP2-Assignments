package cages;

public class IndoorCage {
    private String cageSize;

    public IndoorCage(int length) {
        if (length <= 45) {
            this.cageSize = "A";
        } else if (length >= 60) {
            this.cageSize = "C";
        } else {
            this.cageSize = "B";
        }
    }

    public String getCageSize() {
        return cageSize;
    }

}
