package cages;

public class OutdoorCage {
    private String cageSize;

    public OutdoorCage(int length) {
        if (length <= 75) {
            this.cageSize = "A";
        } else if (length >= 90) {
            this.cageSize = "C";
        } else {
            this.cageSize = "B";
        }
    }

    public String getCageSize() {
        return cageSize;
    }

}
