package animals;

import cages.OutdoorCage;

public class Lion extends Animals {
    private OutdoorCage cage;
    private static int counter = 0;

    public Lion(String name, int length, OutdoorCage cage) {
        super(name, length);
        this.cage = cage;
        counter++;
    }

    public OutdoorCage getCage() {
        return cage;
    }

    public static int getCounter() {
        return counter;
    }

    public void hunting() {
        System.out.println("Lion is hunting..");
        this.makeVoice("err...!");
    }

    public void brush() {
        System.out.println("Clean the lion's mane..");
        this.makeVoice("Hauhhmm!");
    }

    public void disturb() {
        this.makeVoice("HAUHHMM!");
    }

}
