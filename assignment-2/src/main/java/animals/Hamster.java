package animals;

import cages.IndoorCage;

public class Hamster extends Animals {
    private IndoorCage cage;
    private static int counter = 0;

    public Hamster(String name, int length, IndoorCage cage) {
        super(name, length);
        this.cage = cage;
        counter++;
    }

    public IndoorCage getCage() {
        return cage;
    }

    public static int getCounter() {
        return counter;
    }

    public void gnaw() {
        this.makeVoice("ngkkrit.. ngkkrrriiit");
    }

    public void run() {
        this.makeVoice("trrr.... trrr...");
    }

}
