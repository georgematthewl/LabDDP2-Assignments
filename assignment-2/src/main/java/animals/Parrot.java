package animals;

import cages.IndoorCage;

public class Parrot extends Animals {
    private IndoorCage cage;
    private static int counter = 0;

    public Parrot(String name, int length, IndoorCage cage) {
        super(name, length);
        this.cage = cage;
        counter++;
    }

    public IndoorCage getCage() {
        return cage;
    }

    public static int getCounter() {
        return counter;
    }

    public void fly() {
        System.out.println("Parrot " + this.getName() + " flies!");
        this.makeVoice("FLYYYY...");
    }

    public void talk(String words) {
        System.out.println(this.getName() + " says: " + words.toUpperCase());
    }

    public void alone() {
        System.out.println(this.getName() + " says: HM?");
    }

}
