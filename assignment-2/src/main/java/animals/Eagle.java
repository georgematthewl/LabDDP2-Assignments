package animals;

import cages.OutdoorCage;

public class Eagle extends Animals {
    private OutdoorCage cage;
    private static int counter = 0;

    public Eagle(String name, int length, OutdoorCage cage) {
        super(name, length);
        this.cage = cage;
        counter++;
    }

    public OutdoorCage getCage() {
        return cage;
    }

    public static int getCounter() {
        return counter;
    }

    public void fly() {
        this.makeVoice("kwaakk....");
        System.out.println("You hurt!");
    }

}
