package animals;

import cages.IndoorCage;
import java.util.Random;

public class Cat extends Animals {
    private IndoorCage cage;
    private String[] voices = { "Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!" };
    private static int counter = 0;

    public Cat(String name, int length, IndoorCage cage) {
        super(name, length);
        this.cage = cage;
        counter++;
    }

    public IndoorCage getCage() {
        return cage;
    }

    public static int getCounter() {
        return counter;
    }

    public void brushFur() {
        System.out.println("Time to clean " + this.getName() + "’s fur");
        this.makeVoice("Nyaaan...");
    }

    public void cuddle() {
        this.makeVoice(randomVoice());
    }

    public String randomVoice() {
        int randomIndex = new Random().nextInt(voices.length);
        return voices[randomIndex];

    }

}
