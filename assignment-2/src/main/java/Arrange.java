import animals.Animals;
import animals.Cat;
import animals.Eagle;
import animals.Hamster;
import animals.Lion;
import animals.Parrot;
import cages.IndoorCage;
import cages.OutdoorCage;
import java.util.ArrayList;

public class Arrange {
    private static final int MANY_LEVELS = 3;

    public static ArrayList<ArrayList<Animals>> arrange(Animals[] listAnimals) {
        ArrayList<ArrayList<Animals>> listLevels = new ArrayList<ArrayList<Animals>>();
        ArrayList<Animals> levelOne = new ArrayList<Animals>();
        ArrayList<Animals> levelTwo = new ArrayList<Animals>();
        ArrayList<Animals> levelThree = new ArrayList<Animals>();
        int manyAnimals = listAnimals.length;
        if (manyAnimals >= MANY_LEVELS) {
            int firstLevelCage = manyAnimals / MANY_LEVELS;
            for (int i = 0; i < firstLevelCage; i++) {
                levelOne.add(listAnimals[i]);
            }
            int animalsLeft = manyAnimals - firstLevelCage;
            int secondLevelCage = animalsLeft / (MANY_LEVELS - 1);
            for (int j = secondLevelCage; j < firstLevelCage + secondLevelCage; j++) {
                levelTwo.add(listAnimals[j]);
            }
            for (int k = firstLevelCage + secondLevelCage; k < manyAnimals; k++) {
                levelThree.add(listAnimals[k]);
            }
        } else if (manyAnimals == 2) {
            levelOne.add(listAnimals[0]);
            levelTwo.add(listAnimals[1]);
        } else if (manyAnimals == 1) {
            levelOne.add(listAnimals[0]);
        }
        listLevels.add(levelOne);
        listLevels.add(levelTwo);
        listLevels.add(levelThree);
        String cage;
        if (listAnimals[0] instanceof Lion || listAnimals[0] instanceof Eagle) {
            cage = "Outdoor";
        } else {
            cage = "Indoor";
        }
        System.out.println("Location: " + cage);
        print(listLevels);
        return listLevels;
    }

    public static void rearrange(ArrayList<ArrayList<Animals>> unarrangedList) {
        ArrayList<Animals> newLevelOne = new ArrayList<Animals>();
        ArrayList<Animals> newLevelTwo = new ArrayList<Animals>();
        ArrayList<Animals> newLevelThree = new ArrayList<Animals>();

        int lengthLevelOne = unarrangedList.get(2).size();
        int lengthLevelTwo = unarrangedList.get(0).size();
        int lengthLevelThree = unarrangedList.get(1).size();
        for (int i = lengthLevelOne - 1; i >= 0; i--) {
            newLevelOne.add(unarrangedList.get(2).get(i));
        }
        for (int j = lengthLevelTwo - 1; j >= 0; j--) {
            newLevelTwo.add(unarrangedList.get(0).get(j));
        }
        for (int k = lengthLevelThree - 1; k >= 0; k--) {
            newLevelThree.add(unarrangedList.get(1).get(k));
        }
        ArrayList<ArrayList<Animals>> newListLevels = new ArrayList<ArrayList<Animals>>();
        newListLevels.add(newLevelOne);
        newListLevels.add(newLevelTwo);
        newListLevels.add(newLevelThree);
        System.out.println("After rearrangement...");
        print(newListLevels);

    }

    public static void print(ArrayList<ArrayList<Animals>> listInput) {
        for (int i = 3; i > 0; i--) {
            System.out.print("Level " + i + ": ");
            String cage = null;
            int length = 0;
            for (Animals animal : listInput.get(i - 1)) {
                length = animal.getLength();
                if (animal instanceof Lion) {
                    cage = ((Lion) animal).getCage().getCageSize();
                } else if (animal instanceof Cat) {
                    cage = ((Cat) animal).getCage().getCageSize();
                } else if (animal instanceof Eagle) {
                    cage = ((Eagle) animal).getCage().getCageSize();
                } else if (animal instanceof Parrot) {
                    cage = ((Parrot) animal).getCage().getCageSize();
                } else if (animal instanceof Hamster) {
                    cage = ((Hamster) animal).getCage().getCageSize();
                }
                System.out.print(animal.getName() + " (" + length + " - " + cage + "), ");
            }
            System.out.println();
        }
        System.out.println();

    }

}
