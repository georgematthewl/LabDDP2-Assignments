import animals.Animals;
import animals.Cat;
import animals.Eagle;
import animals.Hamster;
import animals.Lion;
import animals.Parrot;
import cages.IndoorCage;
import cages.OutdoorCage;
import java.util.Scanner;

public class JavariPark {
    private static final int MANY_ANIMALS_TYPE = 5;
    private static String[] AnimalsType = { "Cat", "Lion", "Eagle", "Parrot", "Hamster" };

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");

        Animals[][] listAnimals = new Animals[MANY_ANIMALS_TYPE][];
        for (int i = 0; i < AnimalsType.length; i++) {
            System.out.print(AnimalsType[i] + ": ");
            int animals = 0;
            try {
                animals = input.nextInt();
            } catch (Exception e) {
                System.out.println("Wrong Input!");
                break;
            }
            if (animals != 0) {
                listAnimals[i] = new Animals[animals];
                System.out.println("Provide the information of " 
                        + AnimalsType[i].toLowerCase() + "(s): ");
                input.nextLine();
                try {
                    String inputAnimals = input.nextLine();
                    String[] inputAnimalsSplit = inputAnimals.split(",");
                    for (int j = 0; j < animals; j++) {
                        String[] eachAnimalSplit = inputAnimalsSplit[j].split("\\|");
                        String name = eachAnimalSplit[0];
                        int length = Integer.parseInt(eachAnimalSplit[1]);
                        if (i == 0) {
                            Cat newCat = new Cat(name, length, new IndoorCage(length));
                            listAnimals[i][j] = newCat;
                        } else if (i == 1) {
                            Lion newLion = new Lion(name, length, new OutdoorCage(length));
                            listAnimals[i][j] = newLion;
                        } else if (i == 2) {
                            Eagle newEagle = new Eagle(name, length, new OutdoorCage(length));
                            listAnimals[i][j] = newEagle;
                        } else if (i == 3) {
                            Parrot newParrot = new Parrot(name, length, new IndoorCage(length));
                            listAnimals[i][j] = newParrot;
                        } else {
                            Hamster newHamster = new Hamster(name, length, new IndoorCage(length));
                            listAnimals[i][j] = newHamster;
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Wrong input!");
                    return;
                }

            }
        }
        System.out.println("Animals have been successfully recorded!");
        System.out.println("=============================================");
        System.out.println("Cage arrangement: ");
        for (int i = 0; i < MANY_ANIMALS_TYPE; i++) {
            if (listAnimals[i] != null) {
                Arrange.rearrange(Arrange.arrange(listAnimals[i]));
            }
        }
        System.out.println("ANIMALS NUMBER:");
        System.out.println("Cat: " + Cat.getCounter());
        System.out.println("Lion: " + Lion.getCounter());
        System.out.println("Parrot: " + Parrot.getCounter());
        System.out.println("Eagle: " + Eagle.getCounter());
        System.out.println("Hamster: " + Hamster.getCounter());
        System.out.println("=============================================");

        while (true) {
            System.out.println(
                    "Which animal you want to visit? \n"
                    + "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            int numberChoosen = input.nextInt();
            if (numberChoosen == 99) {
                System.out.println("Program will exit");
                break;
            } else {
                String name = "";
                int index = 0;
                int sum = 0;
                switch (numberChoosen) {
                    case 1:
                        name = "cat";
                        index = 0;
                        sum = Cat.getCounter();
                        break;
                    case 2:
                        name = "eagle";
                        index = 2;
                        sum = Eagle.getCounter();
                        break;
                    case 3:
                        name = "hamster";
                        index = 4;
                        sum = Hamster.getCounter();
                        break;
                    case 4:
                        name = "parrot";
                        index = 3;
                        sum = Parrot.getCounter();
                        break;
                    case 5:
                        name = "lion";
                        index = 1;
                        sum = Lion.getCounter();
                        break;
                    default:
                        System.out.println("Wrong command");
                        break;
                }
                if (sum > 0) {
                    System.out.print("Mention the name of " + name + " you want to visit: ");
                    input.nextLine();
                    String nameAnimal = input.nextLine();
                    boolean chosen = false;
                    Animals animalChosen = null;
                    for (Animals animal : listAnimals[index]) {
                        if (animal.getName().equalsIgnoreCase(nameAnimal)) {
                            chosen = true;
                            animalChosen = animal;
                        }
                    }
                    if (chosen) {
                        System.out.println("You are visiting " + animalChosen.getName() 
                                + " (" + name + ") now, what would you like to do?");

                        switch (index) {
                            case 0:
                                System.out.println("1: Brush the fur 2: Cuddle");
                                int choose = input.nextInt();
                                switch (choose) {
                                    case 1:
                                        ((Cat) animalChosen).brushFur();
                                        break;
                                    case 2:
                                        ((Cat) animalChosen).cuddle();
                                        break;
                                    default:
                                        System.out.println("You do nothing...");
                                        break;
                                }
                                break;
                            case 2:
                                System.out.println("1: Order to fly");
                                int choose1 = input.nextInt();
                                switch (choose1) {
                                    case 1:
                                        ((Eagle) animalChosen).fly();
                                        break;
                                    default:
                                        System.out.println("You do nothing...");
                                        break;
                                }
                                break;
                            case 4:
                                System.out.println("1: See it gnawing 2: Order to run in"
                                        + " the hamster wheel");
                                int choose2 = input.nextInt();
                                switch (choose2) {
                                    case 1:
                                        ((Hamster) animalChosen).gnaw();
                                        break;
                                    case 2:
                                        ((Hamster) animalChosen).run();
                                        break;
                                    default:
                                        System.out.println("You do nothing");
                                        break;
                                }
                                break;
                            case 3:
                                System.out.println("1: Order to fly 2: Do conversation");
                                int choose3 = input.nextInt();
                                switch (choose3) {
                                    case 1:
                                        ((Parrot) animalChosen).fly();
                                        break;
                                    case 2:
                                        System.out.print("You say: ");
                                        input.nextLine();
                                        String word = input.nextLine();
                                        ((Parrot) animalChosen).talk(word);
                                        break;
                                    default:
                                        ((Parrot) animalChosen).alone();
                                        break;
                                }
                                break;
                            case 1:
                                System.out.println("1: See it hunting 2: Brush the mane "
                                        + "3: Disturb it");
                                int choose4 = input.nextInt();
                                switch (choose4) {
                                    case 1:
                                        ((Lion) animalChosen).hunting();
                                        break;
                                    case 2:
                                        ((Lion) animalChosen).brush();
                                        break;
                                    case 3:
                                        ((Lion) animalChosen).disturb();
                                        break;
                                    default:
                                        System.out.println("You do nothing");
                                        break;
                                }
                                break;
                            default:
                                System.out.println("You choose nothing");
                                break;
                        }
                        System.out.println("Back to office!");
                    } else {
                        System.out.println("There is no " + name + " with that name!" 
                                + " Back to office!");
                    }
                } else if (numberChoosen <= 0 || numberChoosen >= MANY_ANIMALS_TYPE) {
                    System.out.println("Back to office!");
                } else {
                    System.out.println("There are no " + name + " in this park");
                }
            }
        }
        input.close();
    }

}
