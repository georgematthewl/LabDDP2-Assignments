package javari.animal.mammals;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

/**
 * This class represents common attributes and behaviours found in Mammal in Javari Park. 
 * This class extends superclass Animal.
 *
 * @author georgematthewl
 */

public class Mammals extends Animal {
    String specialStatus;

    /**
     * Constructs instance of Mammals by calling superclass constructor.
     * 
     * @param info array info from input containing animal's info.
     */
    
    public Mammals(String[] info) {
        super(Integer.parseInt(info[0]), info[1], info[2], Gender.parseGender(info[3]), 
                Double.parseDouble(info[4]), Double.parseDouble(info[5]),
                Condition.parseCondition(info[7]));
        this.specialStatus = info[6];
    }

    /**
     * This method implements Specific Condition.
     * @see javari.animal.Animal#specificCondition()
     */
    
    @Override
    public boolean specificCondition() {
        return !(specialStatus.equalsIgnoreCase("Pregnant")
                || (this.getType().equalsIgnoreCase("Lion") && this.getGender() == Gender.FEMALE));
    }
}
