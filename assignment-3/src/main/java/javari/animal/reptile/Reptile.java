package javari.animal.reptile;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

/**
 * This class represents common attributes and behaviors found in Reptile in Javari Park. 
 * This class extends superclass Animal.
 *
 * @author georgematthewl
 */

public class Reptile extends Animal {
    String specialStatus;

    /**
     * Constructs instance of Reptile by calling superclass constructor.
     * 
     * @param info array info from input containing animal's info.
     */
    
    public Reptile(String[] info) {
        super(Integer.parseInt(info[0]), info[1], info[2], 
                Gender.parseGender(info[3]), Double.parseDouble(info[4]),
                Double.parseDouble(info[5]), Condition.parseCondition(info[7]));
        this.specialStatus = info[6];
    }

    /**
     * This method implements SpecificCondition.
     * 
     * @see javari.animal.Animal#specificCondition()
     */
    
    @Override
    public boolean specificCondition() {
        return !(this.specialStatus.equalsIgnoreCase("Tame"));
    }
}
