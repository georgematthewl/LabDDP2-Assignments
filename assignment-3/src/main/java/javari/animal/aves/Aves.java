package javari.animal.aves;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

/**
 * This class represents common attributes and behaviors found in Aves in Javari Park. 
 * This class extends superclass Animal.
 *
 * @author georgematthewl
 */

public class Aves extends Animal {
    String specialStatus;

    /**
     * Construts instance of Aves by calling superclass constructor.
     * 
     * @param info  contain animal's info in array.
     */

    public Aves(String[] info) {
        super(Integer.parseInt(info[0]), info[1], info[2], Gender.parseGender(info[3]), 
                Double.parseDouble(info[4]), Double.parseDouble(info[5]),
                Condition.parseCondition(info[7]));
        this.specialStatus = info[6];
    }

    /**
     * This method implements Specific Condition.
     * @see javari.animal.Animal#specificCondition()
     */
    
    @Override
    public boolean specificCondition() {
        return !(this.specialStatus.equalsIgnoreCase("Laying Eggs"));
    }
}
