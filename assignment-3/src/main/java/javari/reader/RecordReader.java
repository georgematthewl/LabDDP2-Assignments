package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import javari.park.park.Park;

/**
 * This class represents RecordReader to read animal's info from input file.
 * This class extends abstract class CsvReader.
 * 
 * @author georgematthewl
 */

public class RecordReader extends CsvReader {
    int invalid = 0;
    int valid = 0;

    /**
     * Constructs RecordReader by calling constructor of superclass.
     */

    public RecordReader(Path file) throws IOException {
        super(file);
    }

    /**
     * Return the numbers of valid input on the input file.
     * 
     * @return
     */

    public long countValidRecords() {
        for (String line : lines) {
            String[] info = line.split(",");
            try {
                if (Park.getMammal().contains(info[1]) || Park.getAves().contains(info[1])
                        || Park.getReptile().contains(info[1])) {
                    Park.addAnimal(info);
                    valid++;
                } else {
                    invalid++;
                }
            } catch (Exception e) {
                invalid++;
            }
        }
        return valid;
    }

    /**
     * Return the numbers of invalid input on the input file.
     * 
     * @return
     */

    public long countInvalidRecords() {
        return invalid;
    }
}
