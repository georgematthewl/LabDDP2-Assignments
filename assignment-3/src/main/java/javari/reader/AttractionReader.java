package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import javari.park.park.Park;

/**
 * This class represents AttractionReader to read attraction from input file.
 * This class extends abstract class CsvReader
 * 
 * @author georgematthewl
 */
public class AttractionReader extends CsvReader {
    private static String[] validAttraction = { "Circles Of Fires", 
        "Dancing Animals", "Counting Masters",
        "Passionate Coders" };
    private static String[][] validPerformers = { { "Whale", "Lion", "Eagle" }, 
        { "Cat", "Snake", "Parrot", "Hamster" },
        { "Hamster", "Whale", "Parrot" }, { "Cat", "Hamster", "Snake" } };

    /**
     * Constructs AttractionReader by calling constructor of superclass.
     */

    public AttractionReader(Path file) throws IOException {
        super(file);
    }

    /**
     * Return the numbers of valid input on the input file.
     * 
     * @return
     */

    public long countValidRecords() {
        ArrayList<String> valid = new ArrayList<String>();
        for (String line : lines) {
            String[] info = line.split(",");
            for (int i = 0; i < 4; i++) {
                if (info[1].equalsIgnoreCase(validAttraction[i])) {
                    for (String performer : validPerformers[i]) {
                        if (info[0].equalsIgnoreCase(performer)) {
                            Park.addPerformer(validAttraction[i], performer);
                            if (!valid.contains(validAttraction[i])) {
                                valid.add(validAttraction[i]);
                            }
                        }
                    }
                }
            }
        }
        return valid.size();
    }

    /**
     * Return the numbers of invalid input on the input file.
     * 
     * @return
     */

    public long countInvalidRecords() {
        int invalid = 0;
        for (String line : lines) {
            String[] info = line.split(",");
            boolean valid = false;
            for (int i = 0; i < 4; i++) {
                if (info[1].equalsIgnoreCase(validAttraction[i])) {
                    for (String performer : validPerformers[i]) {
                        if (info[0].equalsIgnoreCase(performer)) {
                            valid = true;
                            break;
                        }
                    }
                }
            }
            if (!valid) {
                invalid++;
            }
        }
        return invalid;
    }
}
