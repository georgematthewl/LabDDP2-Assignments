package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import javari.park.park.Park;

/**
 * This class represents CategoriesReader to read animal's section and category from input file. 
 * This class extends abstract class CsvReader.
 * 
 * @author georgematthewl
 */

public class CategoryReader extends CsvReader {
    private static String[] validSection = { "Explore the Mammals", "World of Aves", 
        "Reptillian Kingdom" };
    private static String[] validCategory = { "Mammals", "Aves", "Reptiles" };
    private static String[][] validType = { { "Whale", "Lion", "Cat", "Hamster" }, 
        { "Parrot", "Eagle" }, { "Snake" } };

    /**
     * Constructs AttractionReader by calling constructor of superclass.
     */

    public CategoryReader(Path file) throws IOException {
        super(file);
    }

    /**
     * Return the numbers of valid input on the input file.
     * 
     * @return
     */

    public long countValidRecords() {
        for (String line : lines) {
            String[] info = line.split(",");
            for (int i = 0; i < 3; i++) {
                if (info[2].equalsIgnoreCase(validSection[i]) 
                        && info[1].equalsIgnoreCase(validCategory[i])) {
                    for (int j = 0; j < validType[i].length; j++) {
                        if (info[0].equalsIgnoreCase(validType[i][j])) {
                            Park.addSection(validSection[i]);
                            Park.addType(validSection[i], validType[i][j]);
                        }
                    }
                }
            }
        }
        return Park.getSection().size();
    }

    /**
     * Return the numbers of invalid input on the input file.
     * 
     * @return
     */

    public long countInvalidRecords() {
        int invalid = 0;
        for (String line : lines) {
            String[] info = line.split(",");
            boolean valid = false;
            for (int i = 0; i < 3; i++) {
                if (info[2].equalsIgnoreCase(validSection[i]) 
                        && info[1].equalsIgnoreCase(validCategory[i])) {
                    for (int j = 0; j < validType[i].length; j++) {
                        if (info[0].equalsIgnoreCase(validType[i][j])) {
                            valid = true;
                            break;
                        }
                    }
                }
            }
            if (!valid) {
                invalid++;
            }
        }
        return invalid;
    }
}
