package javari.park.visitor;

import java.util.ArrayList;
import java.util.List;
import javari.park.Registration;
import javari.park.SelectedAttraction;

public class Visitor implements Registration {
    private String name;
    private static int idCount = 0;
    private int id;
    private List<SelectedAttraction> selectedAttraction = new ArrayList<SelectedAttraction>();

    /**
     * Constructs instance of Visitor.
     */
    
    public Visitor() {
        idCount++;
        this.id = idCount;
    }

    @Override
    public String setVisitorName(String name) {
        this.name = name;
        return name;
    }

    @Override
    public String getVisitorName() {
        return this.name;
    }

    @Override
    public int getRegistrationId() {
        return this.id;
    }

    /**
     * The method below implements method from Selected Attraction.
     */
    
    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return this.selectedAttraction;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        this.selectedAttraction.add(selected);
        return true;
    }
}