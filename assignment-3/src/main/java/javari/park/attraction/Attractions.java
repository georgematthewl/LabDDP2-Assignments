package javari.park.attraction;

import java.util.ArrayList;
import java.util.List;
import javari.animal.Animal;
import javari.park.SelectedAttraction;

/**
 * This class represents common attributes of attraction in Javari Park. 
 * This class implements interface SelectedAttraction.
 *
 * @author georgematthewl
 */
public class Attractions implements SelectedAttraction {
    private String name;
    private String type;
    private List<Animal> performers = new ArrayList<Animal>();

    /**
     * Constructs an instance of {@code Attraction}.
     *
     * @param name  name of attraction.
     * @param type  type of animal that perform the attraction.
     */
    public Attractions(String name, String type) {
        this.name = name;
        this.type = type;
    }
    
    /**
     * This method below implements method in selected attraction.
     */

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public List<Animal> getPerformers() {
        return this.performers;
    }

    @Override
    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {
            this.performers.add(performer);
            return true;
        }
        return false;
    }
}
