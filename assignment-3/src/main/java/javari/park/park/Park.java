package javari.park.park;

import java.util.ArrayList;
import javari.animal.Animal;
import javari.animal.aves.Aves;
import javari.animal.mammals.Mammals;
import javari.animal.reptile.Reptile;

/**
 * This class represents the valid section, valid animal, list of animal, list
 * of animal that perform certain attraction in Javari Park.
 * 
 * @author georgematthewl
 */

public class Park {
    private static ArrayList<String> section = new ArrayList<String>();
    private static ArrayList<String> mammals = new ArrayList<String>();
    private static ArrayList<String> aves = new ArrayList<String>();
    private static ArrayList<String> reptile = new ArrayList<String>();
    private static ArrayList<Animal> listOfAnimal = new ArrayList<Animal>();
    private static ArrayList<String> circleOfFire = new ArrayList<String>();
    private static ArrayList<String> dancingAnimals = new ArrayList<String>();
    private static ArrayList<String> countingMasters = new ArrayList<String>();
    private static ArrayList<String> passionateCoders = new ArrayList<String>();

    /**
     * Add valid section to Javari Park.
     */

    public static void addSection(String sec) {
        if (!section.contains(sec)) {
            section.add(sec);
        }
    }

    public static ArrayList<String> getSection() {
        return section;
    }

    public static ArrayList<String> getCof() {
        return circleOfFire;
    }

    public static ArrayList<String> getDa() {
        return dancingAnimals;
    }

    public static ArrayList<String> getCm() {
        return countingMasters;
    }

    public static ArrayList<String> getPc() {
        return passionateCoders;
    }

    public static ArrayList<Animal> getListOfAnimal() {
        return listOfAnimal;
    }

    public static ArrayList<String> getMammal() {
        return mammals;
    }

    public static ArrayList<String> getAves() {
        return aves;
    }

    public static ArrayList<String> getReptile() {
        return reptile;
    }

    /**
     * Add the type of animal to each appropriate category.
     */

    public static void addType(String sec, String type) {
        if (sec.equalsIgnoreCase("Explore the Mammals") && !mammals.contains(type)) {
            mammals.add(type);
        } else if (sec.equalsIgnoreCase("World of Aves") && !aves.contains(type)) {
            aves.add(type);
        } else if (sec.equalsIgnoreCase("Reptillian Kingdom") && !reptile.contains(type)) {
            reptile.add(type);
        }
    }


    /**
     * Method to add animal to Javari Park.
     */

    public static void addAnimal(String[] info) {
        if (info[1].equalsIgnoreCase("Lion")) {
            listOfAnimal.add(new Mammals(info));
        } else if (info[1].equalsIgnoreCase("Cat")) {
            listOfAnimal.add(new Mammals(info));
        } else if (info[1].equalsIgnoreCase("Hamster")) {
            listOfAnimal.add(new Mammals(info));
        } else if (info[1].equalsIgnoreCase("Whale")) {
            listOfAnimal.add(new Mammals(info));
        } else if (info[1].equalsIgnoreCase("Parrot")) {
            listOfAnimal.add(new Aves(info));
        } else if (info[1].equalsIgnoreCase("Eagle")) {
            listOfAnimal.add(new Aves(info));
        } else if (info[1].equalsIgnoreCase("Snake")) {
            listOfAnimal.add(new Reptile(info));
        }
    }

    /**
     * Method to add performer to corresponding attraction.
     */

    public static void addPerformer(String attraction, String performer) {
        if (attraction.equalsIgnoreCase("Circles of Fires")) {
            circleOfFire.add(performer);
        }
        if (attraction.equalsIgnoreCase("Dancing Animals")) {
            dancingAnimals.add(performer);
        }
        if (attraction.equalsIgnoreCase("Counting Masters")) {
            countingMasters.add(performer);
        }
        if (attraction.equalsIgnoreCase("Passionate Coders")) {
            passionateCoders.add(performer);
        }
    }

}
