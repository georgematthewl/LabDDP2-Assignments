package javari;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import javari.animal.Animal;
import javari.park.attraction.Attractions;
import javari.park.park.Park;
import javari.park.visitor.Visitor;
import javari.reader.AttractionReader;
import javari.reader.CategoryReader;
import javari.reader.CsvReader;
import javari.reader.RecordReader;
import javari.writer.RegistrationWriter;

/**
 * This is main class A3Festival, this class run the program of Javari Park
 * Festival.
 * 
 * @author georgematthewl
 * @version 1.4 DDP2 - C
 * 
 */

public class A3Festival {
    static Scanner input = new Scanner(System.in);
    static Visitor visitor = new Visitor();

    /**
     * This method is the main of Javari Park This method function is to read input
     * of animals_categories, animals_records, and animals_attractions from user.
     */

    public static void main(String[] args) {
        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        String defaultPath = "/Users/ronnylimongan/Documents/SEMESTER 2/TP-DDP2/assignment-3/data";
        CsvReader[] files = new CsvReader[3];

        while (true) {
            try {
                files[0] = new CategoryReader(Paths.get(defaultPath, "animals_categories.csv"));
                files[1] = new AttractionReader(Paths.get(defaultPath, "animals_attractions.csv"));
                files[2] = new RecordReader(Paths.get(defaultPath, "animals_records.csv"));
                System.out.println("\n... Loading... Success... System is populating data...\n");
                break;
            } catch (IOException e) {
                System.out.println("\n... File not found or incorrect file!\n");
                System.out.println("Please provide the source data path: ");
                defaultPath = input.nextLine();
            }
        }
        long validSection = files[0].countValidRecords();
        long invalidSection = files[0].countInvalidRecords();
        System.out.printf("Found %d valid sections and %d invalid section %n", 
                validSection, invalidSection);
        System.out.printf("Found %d valid attractions and %d invalid attractions%n", 
                files[1].countValidRecords(), files[1].countInvalidRecords());
        System.out.printf("Found %d valid animal categories and %d invalid animal categories%n", 
                validSection, invalidSection);
        System.out.printf("Found %d valid animal records and %d invalid animal records%n", 
                files[2].countValidRecords(), files[2].countInvalidRecords());
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("Please answer the questions by typing the number. ");
        System.out.println("Type # if you want to return to the previous menu\n");
        chooseSection();
    }

    /**
     * This method function is to print the available section in Javari Park and to
     * read section wanted by the visitor.
     */

    public static void chooseSection() {
        System.out.println("Javari Park has 3 sections:");
        int sect = 1;
        for (String section : Park.getSection()) {
            System.out.println(sect + ". " + section);
            sect++;
        }
        System.out.print("Please choose your preferred section (type the number): ");
        String choice = input.nextLine();
        try {
            if (choice.equals("#")) {
                chooseSection();
            } else if (Integer.parseInt(choice) <= Park.getSection().size() 
                    && Integer.parseInt(choice) > 0) {
                chooseAnimal(Park.getSection().get(Integer.parseInt(choice) - 1));
            } else {
                System.out.println("Wrong input!");
                chooseSection();
            }
        } catch (Exception e) {
            System.out.println("Wrong input!");
            chooseSection();
        }
    }

    /**
     * This method function is to print the available animal in the section that is
     * chosen before and to read animal wanted by the visitor.
     * 
     * @param section section that is chosen before.
     */

    public static void chooseAnimal(String section) {
        String newSection = section;
        System.out.println("--" + section + "--");
        ArrayList<String> animals = new ArrayList<String>();
        switch (section) {
            case "Explore the Mammals":
                animals = Park.getMammal();
                break;
            case "World of Aves":
                animals = Park.getAves();
                break;
            case "Reptillian Kingdom":
                animals = Park.getReptile();
                break;
            default:
                System.out.println("Section not found!");
                chooseAnimal(newSection);
        }
        int number = 1;
        for (String animal : animals) {
            System.out.println(number + ". " + animal);
            number++;
        }

        System.out.print("Please choose your preferred animal (type the number): ");
        String choice = input.nextLine();
        try {
            if (choice.equals("#")) {
                chooseSection();
            } else if (Integer.parseInt(choice) <= animals.size() && Integer.parseInt(choice) > 0) {
                boolean avail = false;
                String animalChoice = animals.get(Integer.parseInt(choice) - 1);
                for (Animal a : Park.getListOfAnimal()) {
                    if (a.getType().equalsIgnoreCase(animalChoice) && a.isShowable()) {
                        avail = true;
                        break;
                    }
                }
                if (avail) {
                    chooseAttraction(section, animalChoice);
                } else {
                    System.out.println("Unfortunately, no " + animalChoice
                            + " can perform any attraction, please choose other animals");
                    chooseAnimal(newSection);
                }
            } else {
                System.out.println("Wrong input!");
                chooseAnimal(newSection);
            }
        } catch (Exception e) {
            System.out.println("Wrong input!");
            chooseAnimal(newSection);
        }

    }

    /**
     * This method function is to print the available attraction by the choosen
     * animal and to read attraction wanted by the visitor.
     * 
     * @param section  the section of the animal
     * @param animal   animal that is choosen before
     */

    public static void chooseAttraction(String section, String animal) {
        String newSect = section;
        String newAnimal = animal;
        System.out.println("Attraction(s) by " + animal);
        ArrayList<String> availAtt = new ArrayList<String>();
        if (Park.getCof().contains(animal)) {
            availAtt.add("Circles Of Fires");
        }
        if (Park.getDa().contains(animal)) {
            availAtt.add("Dancing Animals");
        }
        if (Park.getCm().contains(animal)) {
            availAtt.add("Counting Masters");
        }
        if (Park.getPc().contains(animal)) {
            availAtt.add("Passionate Coders");
        }

        int number = 1;
        for (String att : availAtt) {
            System.out.println(number + ". " + att);
            number++;
        }

        System.out.println("Please choose your preferred attractions (type the number): ");
        String choice = input.nextLine();
        try {
            if (choice.equals("#")) {
                chooseAnimal(section);
            } else if (Integer.parseInt(choice) <= availAtt.size()
                    && Integer.parseInt(choice) > 0) {
                Attractions attraction = new Attractions(availAtt.get(
                        Integer.parseInt(choice) - 1), animal);
                for (Animal an : Park.getListOfAnimal()) {
                    if (an.getType().equalsIgnoreCase(animal)) {
                        attraction.addPerformer(an);
                    }
                }
                inputData(attraction);
            } else {
                System.out.println("Wrong input!");
                chooseAttraction(newSect, newAnimal);
            }
        } catch (Exception e) {
            System.out.println("Wrong input!");
            chooseAttraction(newSect, newAnimal);
        }

    }

    /**
     * This method function is to read the visitor data.
     * 
     * @param attraction  attraction wanted by the visitor.
     */

    public static void inputData(Attractions attraction) {
        System.out.println("Wow, one more step!");
        System.out.print("Please let us know your name: ");
        String name = input.nextLine();
        visitor.setVisitorName(name);
        lastCheck(visitor, attraction);
    }

    /**
     * This method function is to confirm the registration.
     * 
     * @param visitor    the Visitor
     * @param attraction the attraction chosen
     */

    public static void lastCheck(Visitor visitor, Attractions attraction) {
        System.out.println("Yeay, last check!");
        System.out.println("Here is your data, and the attraction you chose:");
        System.out.println("Name: " + visitor.getVisitorName());
        System.out.println("Attraction: " + attraction.getName() + " -> " + attraction.getType());
        String performer = "";
        for (Animal a : attraction.getPerformers()) {
            performer += a.getName() + ", ";
        }
        System.out.println("With: " + performer.substring(0, performer.length() - 2));

        System.out.print("Is the data correct? (Y/N): ");
        if (input.nextLine().equalsIgnoreCase("Y")) {
            visitor.addSelectedAttraction(attraction);
            chooseAnother();
        } else if (input.nextLine().equalsIgnoreCase("N")) {
            chooseSection();
        } else {
            System.out.println("Wrong input!");
            chooseSection();
        }
    }

    /**
     * This method function is to check whether the visitor want to choose another
     * attraction or not.
     */

    public static void chooseAnother() {
        System.out.println("Thank you for your interest. ");
        System.out.println("Would you like to register to other attractions? (Y/N): ");
        if (input.nextLine().equalsIgnoreCase("Y")) {
            chooseSection();
        } else if (input.nextLine().equalsIgnoreCase("N")) {
            printRegistration();
        } else {
            System.out.println("Wrong input!");
            chooseSection();
        }
    }

    /**
     * This method function is to print the registration data to JSON file.
     */

    public static void printRegistration() {
        String defaultPath = "/Users/ronnylimongan/Documents/SEMESTER 2/TP-DDP2/assignment-3/data";
        try {
            RegistrationWriter.writeJson(visitor, Paths.get(defaultPath));
        } catch (IOException e) {
            System.out.println("\n... Destination folder not found\n");
            System.out.println("Please provide destination folder path: ");
            defaultPath = input.nextLine();
        }
    }
}
