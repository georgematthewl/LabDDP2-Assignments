public class WildCat {

    String name;
    double weight; // In kilograms
    double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public double computeMassIndex() {
        double hitungBmi = weight / Math.pow(length / 100.0, 2);
        return hitungBmi;
    }
}
