import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class A1Station {
    private static ArrayList<TrainCar> listGerbongKereta = new ArrayList<TrainCar>();
    private static ArrayList<TrainCar> listKeretaBerangkat = new ArrayList<TrainCar>();
    private static final double THRESHOLD = 250; // In kilograms
    private static DecimalFormat df2 = new DecimalFormat(".##");

    public static void keretaBerangkat() {
        // Link gerbong kereta
        int lastIndex = listKeretaBerangkat.size() - 1;
        for (int i = lastIndex; i > 0; i--) {
            listKeretaBerangkat.get(i).next = listKeretaBerangkat.get(i - 1);
        }
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        listKeretaBerangkat.get(lastIndex).printCar();
        double averageMassIndex = listKeretaBerangkat.get(lastIndex).computeTotalMassIndex()
                / listKeretaBerangkat.size();
        System.out.println("Average mass index of all cats: " + df2.format(averageMassIndex));
        String category = "";
        if (averageMassIndex < 18.5) {
            category = "underweight";
        } else if (averageMassIndex >= 18.5 && averageMassIndex < 25) {
            category = "normal";
        } else if (averageMassIndex >= 25 && averageMassIndex < 30) {
            category = "overweight";
        } else if (averageMassIndex >= 30) {
            category = "obese";
        }
        System.out.println("In average, the cats in the train are " + "*" + category + "* ");
    }

    public static void cekBeratKereta() {
        /* Melakukan cek berat pada gerbong kereta yang akan berangkat */
        double tempWeight = 0;
        for (TrainCar gerbongKereta : listGerbongKereta) {
            tempWeight += gerbongKereta.computeTotalWeight();
            listKeretaBerangkat.add(gerbongKereta);
            if (tempWeight > THRESHOLD) {
                keretaBerangkat();
                listKeretaBerangkat.clear();
                tempWeight = 0;
            }
        }
        if (listKeretaBerangkat.size() != 0) {
            keretaBerangkat();
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int banyakInput = input.nextInt();
        for (int counter = 0; counter < banyakInput; counter++) {
            String masukan = input.next();
            String[] splitMasukan = masukan.split(",");
            String name = splitMasukan[0];
            int weight = Integer.parseInt(splitMasukan[1]);
            int length = Integer.parseInt(splitMasukan[2]);
            WildCat kucing = new WildCat(name, weight, length); // Membuat objek kucing

            /* Membuat objek kereta dan memasukan kucing ke dalam kereta */
            TrainCar kereta = new TrainCar(kucing);
            listGerbongKereta.add(kereta); // Memasukan ke dalam list gerbong kereta
        }
        cekBeratKereta();
        input.close();
    }
}
