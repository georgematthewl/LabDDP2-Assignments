public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    WildCat cat;
    TrainCar next;

    public TrainCar(WildCat cat) {
        this(cat, null);
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        if (this.next == null) { // base case: ketika kereta udah di gerbong terakhir
            return (EMPTY_WEIGHT + this.cat.weight);
        } else { // recursive case: next buat geser ke kereta berikutnya dan jumlahin
            return (EMPTY_WEIGHT + this.cat.weight) + this.next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {
        if (this.next == null) { // Base case: ketika kereta uda di gerbong terakhir
            return this.cat.computeMassIndex();
        } else { // Recursive case: buat geser ke gerbong kereta selanjutnya
            return this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        // cetak nama kucing yang di dalam kereta
        if (this.next == null) {
            System.out.println("(" + this.cat.name + ") ");
        } else {
            System.out.print("(" + this.cat.name + ")" + "--");
            this.next.printCar();
        }
    }
}
