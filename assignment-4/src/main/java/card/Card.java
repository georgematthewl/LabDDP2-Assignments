package card;

import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * Class that represent Card of the game.
 */

public class Card {
    private final int id;
    private boolean matched = false;
    private final JButton button = new JButton();
    private final ImageIcon imgIcon;
    private final ImageIcon imgPressed;

    /**
     * Create constructor of Card.
     * @param id            id of the card
     * @param imgIcon       cover of the button
     * @param imgPressed    image behind the button
     */

    public Card(int id, ImageIcon imgIcon, ImageIcon imgPressed) {
        this.id = id;
        this.imgIcon = imgIcon;
        this.imgPressed = imgPressed;
        this.button.setIcon(imgIcon);
    }

    /**
     * Accessor to get id of a card.
     * @return id of the card
     */

    public int getId() {
        return this.id;
    }

    /**
     * Accessor of the boolean matched.
     * @return true if matched, false otherwise
     */

    public boolean getMatched() {
        return this.matched;
    }

    /**
     * Mutator of boolean matched.
     * @param matched boolean that is going to be set true or false
     */

    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    /**
     * Accessor for JButton.
     * @return button
     */

    public JButton getButton() {
        return button;
    }

    /**
     * Method to show the image behind the cover.
     */

    public void press() {
        button.setIcon(imgPressed);
    }

    /**
     * Method to show the cover.
     */

    public void release() {
        button.setIcon(imgIcon);
    }

}