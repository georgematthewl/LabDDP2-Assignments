package board;

import card.Card;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.WindowConstants;

/**
 * Class of board game.
 * @version 2.0
 * @author George Matthew Limongan
 */

public class Board extends JFrame {

    private static final int PAIRS = 18;
    private static String OS = System.getProperty("os.name").toLowerCase();
    private final ArrayList<Card> cards;
    private final Timer timer;
    private Card firstCard;
    private Card secondCard;
    private JFrame mainFrame;
    private JLabel tryLabel;
    private JLabel resetLabel;
    private JPanel topPanel;
    private int reset = 0;
    private int tries = 0;


    /**
     * Constructor of Board class.
     */

    public Board() {
        ArrayList<Card> cardsList = new ArrayList<>();
        ArrayList<Integer> cardValues = new ArrayList<>();
        for (int i = 0; i < PAIRS; i++) {
            cardValues.add(i);
            cardValues.add(i);
        }
        Collections.shuffle(cardValues);
        try {
            for (int value : cardValues) {
                String[] animals = {"bee", "bird", "cat", "chicken", "cow", "deer",
                                    "dog", "fish", "elephant", "koala",
                                    "ladybird", "bear", "littlechicken",
                                    "owl", "panda", "penguin", "pig", "whale"};
                String path = "";
                if (isWindows()) {
                    path = "assignment-4\\src\\main\\img\\";
                } else if (isMac() || isUnix()) {
                    path = "assignment-4/src/main/img/";
                }
                Card card = new Card(value, resize(new ImageIcon((path + "cover.png"))),
                        resize(new ImageIcon(path + animals[value] + ".png")));
                card.getButton().addActionListener(actionPerformed -> doTurnCards(card));
                cardsList.add(card);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,
                    "<html>Error<br>Missing images</html>", "Error!", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        this.cards = cardsList;
        // Set up the timer
        timer = new Timer(700, actionPerformed -> checkCards());
        timer.setRepeats(false);
        createPanel();
    }

    /**
     * Method to create panel of the game.
     */

    private void createPanel() {
        mainFrame = new JFrame("Match Pair Memory Game (Zoo Edition)");
        mainFrame.setLayout(new BorderLayout());
        topPanel = new JPanel(new GridLayout(6, 6));
        for (Card card : cards) {
            topPanel.add(card.getButton());
        }
        JPanel bottomPanel = new JPanel(new GridLayout(2, 2));
        JButton resetButton = new JButton("Reset Game");
        JButton quitButton = new JButton("Quit");
        bottomPanel.add(resetButton);
        bottomPanel.add(quitButton);
        resetLabel = new JLabel("Number of Reset: " + reset);
        tryLabel = new JLabel("Number of Tries: " + tries);
        resetLabel.setHorizontalAlignment(SwingConstants.CENTER);
        tryLabel.setHorizontalAlignment(SwingConstants.CENTER);
        bottomPanel.add(resetLabel);
        bottomPanel.add(tryLabel);
        resetButton.addActionListener(actionPerformed -> resetGame(cards));
        quitButton.addActionListener(actionPerformed -> exitGame());
        mainFrame.add(topPanel, BorderLayout.CENTER);
        mainFrame.add(bottomPanel, BorderLayout.SOUTH);
        mainFrame.setPreferredSize(new Dimension(750, 750));
        mainFrame.setLocation(250, 150);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.pack();
        mainFrame.setVisible(true);
        mainFrame.setResizable(false);
        Timer waitTime = new Timer(2000, actionPerformed -> revealAllCards());
        waitTime.setRepeats(false);
        waitTime.start();
    }


    /**
     * Method to exit the game when user press the quit button.
     */

    private void exitGame() {
        if (JOptionPane.showConfirmDialog(null, "Are you sure?",
                "WARNING!", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }

    /**
     * Method to resetGame card.
     *
     * @param cards all cards that will be resetGame
     */

    private void resetGame(ArrayList<Card> cards) {
        mainFrame.remove(topPanel);
        Collections.shuffle(cards);
        topPanel = new JPanel(new GridLayout(6, 6));
        for (Card card : cards) {
            card.setMatched(false);
            card.release();
            card.getButton().setEnabled(true);
            topPanel.add(card.getButton());
        }
        mainFrame.add(topPanel, BorderLayout.CENTER);
        reset++;
        tries = 0;
        resetLabel.setText("Number of Reset: " + reset);
        tryLabel.setText("Number of Tries: " + tries);
        revealAllCards();
    }

    /**
     * Method to reveal all cards at the beginning of the game.
     */

    private void revealAllCards() {
        for (Card card : cards) {
            card.press();
        }
        topPanel.updateUI();
        Timer openTime = new Timer(2500, actionPerformed -> hideAllCards());
        openTime.setRepeats(false);
        openTime.start();
    }

    /**
     * Method to hide all cards after reveal all of the card at beginning.
     */

    private void hideAllCards() {
        for (Card card : cards) {
            card.release();
        }
        topPanel.updateUI();

    }

    /**
     * Method to open up card selected.
     * @param selectedCard  card that is selected
     */

    private void doTurnCards(Card selectedCard) {
        if (firstCard == null && secondCard == null) {
            firstCard = selectedCard;
            firstCard.press();
        }

        if (firstCard != null && firstCard != selectedCard && secondCard == null) {
            secondCard = selectedCard;
            secondCard.press();
            timer.start();
        }
    }

    /**
     * Method to check the cards, whether it match or not.
     * Then reset the reference of first and second card.
     */

    private void checkCards() {
        if (firstCard.getId() == secondCard.getId()) {
            matchButton();
            JLabel messageLabel = new JLabel("Congratulations! You Win the Game!");
            messageLabel.setHorizontalAlignment(SwingConstants.CENTER);
            if (isGameWon()) {
                Object[] choices = { " Quit", " Play Again!" };
                int option = JOptionPane.showOptionDialog(null, messageLabel,
                        "Match Pair Memory Game (Zoo Edition)", JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.PLAIN_MESSAGE, null, choices, choices[1]);
                if (option == 1) {
                    resetGame(cards);
                } else {
                    System.exit(0);
                }
            }
        } else {
            firstCard.release();
            secondCard.release();
            tries++;
            tryLabel.setText("Number of Tries: " + tries);
        }
        firstCard = null;
        secondCard = null;
    }

    /**
     * Method to disable and flags button if the id matched.
     */

    private void matchButton() {
        firstCard.getButton().setEnabled(false);
        secondCard.getButton().setEnabled(false);
        firstCard.setMatched(true);
        secondCard.setMatched(true);
    }

    /**
     * Method to check if player win the game or not.
     * @return true if the game have finished, false otherwise
     */

    private boolean isGameWon() {
        for (Card card : cards) {
            if (!card.getMatched()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to resize the icon to user referred size.
     * @param imageIcon image that want to be resized
     * @return image that already resized
     */

    private static ImageIcon resize(ImageIcon imageIcon) {
        Image image = imageIcon.getImage();
        Image resizedImage = image.getScaledInstance(95, 95, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }

    /**
     * Method to check the OS of user that run the program.
     * @return true if the OS is Windows
     */

    private static boolean isWindows() {
        return (OS.contains("win"));
    }

    /**
     * Method to check the OS of user that run the program.
     * @return true if the OS is macOS
     */

    private static boolean isMac() {
        return (OS.contains("mac"));
    }

    /**
     * Method to check the OS of user that run the program.
     * @return true if the OS is Linux
     */

    private static boolean isUnix() {
        return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix"));
    }


}